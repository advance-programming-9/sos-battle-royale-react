import React from 'react';
import Square from './Square/Square';
import './Square/Square.css';
import './Board.css';


function Board(props) {
    let handleClick = (i, j) => {
        if (props.squares[i][j].hasPressed !== false) return
        let grid = {
            x: i,
            y: j,
            character: props.sIsNext ? "S" : "O",
        }
        let header = {
            "Authorization": "Bearer " + props.jwtToken
        }
        props.onClickBoardSquare()
        props.stompClient.send('/app/game/room/' + props.roomId, header, JSON.stringify(grid))
    }

    let rows = props.squares.map((items, key) => {
        let grid = items.map((button, key2) => {
            return (
                <Square
                    key={(key2 + 1) * 10 + key}
                    whoIsNext={props.whoIsNext}
                    sIsNext={props.sIsNext}
                    username={props.username}
                    gameState={props.gameState}
                    hasPressed={button.hasPressed}
                    value={button.stats}
                    horizontalStrike={button.horizontalStrike}
                    verticalStrike={button.verticalStrike}
                    leftDiagonalStrike={button.leftDiagonalStrike}
                    rightDiagonalStrike={button.rightDiagonalStrike}
                    onclick={() => handleClick(key, key2)}
                />
            )
        })
        return (
            <div key={key} className="board-row">
                {grid}
            </div>
        )
    })

    return (
        <div style={{ height: "88%" }} >
            <div className="play-area">
                {rows}
            </div>
        </div>
    )
}

export default Board;