import React from 'react';

class Square extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            dummySOS: null,
            background: "",
            cursor: "",
        }
    }

    decorateWithStrike = (value) => {
        let decorator;
        let horizontal, vertical, leftDiagonal, rightDiagonal;
        horizontal = this.props.verticalStrike
        vertical = this.props.horizontalStrike
        leftDiagonal = this.props.leftDiagonalStrike
        rightDiagonal = this.props.rightDiagonalStrike

        decorator = (
            <span>
                {value}
            </span>
        )
        if (horizontal) {
            decorator = (
            <div
                className="strike strike-horizontal"
            >
                {decorator}
            </div>)
        }
        if (vertical) {
            decorator = (
            <div
                className="strike strike-vertical"
            >
                {decorator}
            </div>)
        }
        if (leftDiagonal) {
            decorator = (
            <div
                className="strike strike-left-diagonal"
            >
                {decorator}
            </div>)
        }
        if (rightDiagonal) {
            decorator = (
            <div
                className="strike strike-right-diagonal"
            >
                {decorator}
            </div>)
        }
        return decorator
    }

    isValidToClick = () => {
        return this.props.whoIsNext === this.props.username && 
            this.props.hasPressed === false && 
            this.props.gameState === "playing";
    }

    onMouseEnter = (e) => {
        if (this.isValidToClick()) {
            this.setState({ dummySOS: this.props.sIsNext ? 'S' : 'O' })
            this.setState({cursor:'pointer'})
            this.setState({background:'#5CDB95'})
        }
    }

    onMouseLeave = (e) => {
        if (this.isValidToClick()) {
            this.setState({ dummySOS: null })
            this.setState({cursor:'default'})
            this.setState({background:'#05386B'})
        }
    }

    onClick = (e) => {
        if (this.isValidToClick()) {
            this.setState({cursor:'default'})
            this.setState({background:'#05386B'})
            this.setState({ dummySOS: null })
            this.props.onclick()
        }
    }

    componentWillReceiveProps = (props) => {
        if (
                props.whoIsNext !== props.username && 
                props.hasPressed === false && 
                props.gameState === "playing"
            ) {
                this.setState({background:'#05386B', cursor:'default', dummySOS:null})
            }
    }

    render() {
        let value = this.props.value === "X" ? " " : this.props.value
        let decorated = this.decorateWithStrike(value)
        let gridIsBlocked;
        if (!this.isValidToClick()) {
            gridIsBlocked = true;
        }

        let renderedSOS;
        if (value !== " ") {
            renderedSOS = decorated;
        } else {
            if (this.isValidToClick()) {
                renderedSOS = this.state.dummySOS;
            } else {
                renderedSOS = decorated;
            }
        }

        return (
            <div
                className={`block ${ gridIsBlocked ? "disabled" : ""}`}
                onClick={(e) => this.onClick(e)}
                onMouseEnter={(e) => this.onMouseEnter(e)}
                onMouseLeave={(e) => this.onMouseLeave(e)}
                style={
                    {
                        background: this.state.background,
                        cursor: this.state.cursor,
                    }
                }
            >
                {renderedSOS}
            </div>
        )
    };
}

export default Square;