import React, { Component } from 'react'
import ChatInput from './ChatInput'
import ChatMessage from './ChatMessage'
import './Chat.css'

export class Chat extends Component {

    sendMessage = (messageString) => {
        const message = { 
            name: this.props.username, 
            message: messageString 
        }

        let header = {
            "Authorization" : "Bearer " + this.props.jwtToken
        }

        this.props.stompClient.send("/app/message/room/" + this.props.roomId, header, JSON.stringify(message));
    }

    componentDidUpdate = () => {
        let obj = document.getElementById("all-chats")
        obj.scrollTop = obj.scrollTopMax
    }

    render() {
        return (
            <div className="chat-card">
                <div id="all-chats" className="all-chats">
                    {this.props.messages.map((message, index) =>
                        <ChatMessage
                            key={index}
                            message={message.message}
                            name={message.name}
                        />,
                    )}
                </div>
                <div className="input-chat">
                    <ChatInput
                        onSubmitMessage={messageString => this.sendMessage(messageString)}
                    />
                </div>
            </div>
        )
    }
}

export default Chat