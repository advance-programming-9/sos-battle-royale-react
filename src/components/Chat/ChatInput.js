import React from 'react';
import PropTypes from 'prop-types'

class ChatInput extends React.Component {
    static propTypes = {
        onSubmitMessage: PropTypes.func.isRequired,
    }
    state = {
        message: '',
    }

    render() {
        return (
            <form
                action="."
                onSubmit={e => {
                    e.preventDefault()
                    this.props.onSubmitMessage(this.state.message)
                    this.setState({ message: '' })
                }}
            >
                <div className="the-input">
                    <input
                        type="text"
                        placeholder={'Type something here...'}
                        value={this.state.message}
                        onChange={e => this.setState({ message: e.target.value })}
                    />
                </div>
                <div className="the-button">
                    <button className="btn primary-button" type="submit" >Send</button>
                </div>
            </form>
        )
    }
}

export default ChatInput;