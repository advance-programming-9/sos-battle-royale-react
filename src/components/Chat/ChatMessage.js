import React from 'react'

export default ({ name, message }) =>
<div>
    <hr />
    <div>
        <strong>{name}:</strong>{message}
    </div>
</div>