import React, { Component } from 'react'
import { Navbar, Nav, Dropdown } from 'react-bootstrap'
import { API_ROOT } from '../../config/api-config'
import Loader from 'react-loader-spinner';

import Room from '../Room/Room'
import Auth from '../Auth/Auth'

import '../Auth/Auth.css'
import './Core.css'

export class Core extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isValidated: null,
            errorUsername: null,
            errorEmail: null,
            errorPassword: null,
            chooseLogin: true,
            invalidLogin: null,
            jwtToken: null,
            roomId: -1,
            username: null,
        }

    }

    handleChooseLoginOrRegister = (e) => {
        this.setState({
            chooseLogin: !this.state.chooseLogin
        })
    }

    componentDidMount = () => {
        this.validateJWT();
    }

    handleSubmitForm = (jsonform, endpoint) => {
        fetch(`${API_ROOT}/user/${endpoint}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(jsonform),
        })
            .then(res => res.json())
            .then(
                (response) => {
                    this.setState({
                        errorUsername: response.usernameError,
                        errorEmail: response.emailError,
                        errorPassword: response.passwordError,
                        errorPassword2: response.passwordError2,
                        invalidLogin: response.invalidLogin,
                        jwtToken: response.jwtToken,
                    })
                    if (response.isValid === true) {
                        this.setState({ chooseLogin: true })
                    }
                    if (response.jwtToken !== undefined && response.jwtToken !== null) {
                        sessionStorage.setItem("__session", this.state.jwtToken)
                        this.setState({ isValidated: true })
                    }
                    this.fetchMatch()
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    validateJWT = () => {
        const jwt = sessionStorage.getItem("__session")
        if (jwt === null) {
            this.setState({ isValidated: false })
            return;
        }
        fetch(`${API_ROOT}/user/jwt/validate`, {
            method: 'POST',
            headers: {
                'Authorization': 'Bearer ' + jwt,
                'Content-Type': 'application/json',
            }
        })
            .then(res => res.json())
            .then(
                (response) => {
                    this.setState({
                        isValidated: response.isValidated,
                        jwtToken: response.refreshedToken,
                    })
                    if (response.isValidated === true) {
                        this.fetchMatch()
                        sessionStorage.setItem("__session", response.refreshedToken)
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    fetchMatch = () => {
        const jwt = sessionStorage.getItem("__session")
        fetch(`${API_ROOT}/user/match`, {
            headers: {
                'Authorization': 'Bearer ' + jwt,
                'Content-Type': 'application/json',
            }
        })
            .then(res => res.json())
            .then(
                (res) => {
                    this.setState({
                        roomId: res.roomId === null ? -1 : res.roomId,
                        username: res.username,
                    })
                },
                (err) => {
                    console.log(err);
                }
            )
    }

    onEnterRoom = (roomId) => {
        if (this.state.roomId === -1) {
            this.setState({
                roomId: roomId,
            })
        }
    }

    onLeaveRoom = () => {
        this.setState({
            roomId: -1,
        })
    }

    render() {
        return (
            <div className="primary-background">
                <div id="loading" className={this.state.isValidated ? 'loading complete' : 'loading'}>
                    {
                        this.state.isValidated === null || this.state.isValidated === true ?
                            <Loader id="loader" type='Oval' color="#2BAD60" width={150} height={150} />
                            :
                            <div className="wrapper">
                                <div className="card" >
                                    <Auth
                                        onLoginForm={(jsonForm, endpoint) => this.handleSubmitForm(jsonForm, endpoint)}
                                        handleChooseLoginOrRegister={e => this.handleChooseLoginOrRegister(e)}
                                        usernameErrorMsg={this.state.errorUsername}
                                        emailErrorMsg={this.state.errorEmail}
                                        passwordErrorMsg={this.state.errorPassword}
                                        chooseLogin={this.state.chooseLogin}
                                        invalidLogin={this.state.invalidLogin}
                                    />
                                </div>
                            </div>
                    }
                </div>
                {
                    (this.state.roomId === null || this.state.roomId === undefined || this.state.roomId === -1) ? 
                    <Navbar className="fixed-top" id="navbar" collapseOnSelect expand="lg" bg="dark" variant="dark">
                        <button style={{ color: "#8EE4AF" }} className="navbar-sos btn navbar-brand nav-link">The S|O|S
                        <div style={{display: "inline-block", marginLeft: "20px"}} >
                            <img alt="SOS" style={{width: "23px"}} src="logo-sos.svg"></img>
                        </div>
                        </button>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto">
                            </Nav>
                            <Nav>
                                <div className="text-center play-text">Play</div>
                                <Dropdown>
                                    <Dropdown.Toggle id="dropdown-basic" className="">
                                        {this.state.username}
                                        <div style={{display: "inline-block", margin: "0 10px 0 10px"}} >
                                            <img alt="" style={{width: "28px"}} src="dummy-profile.svg" ></img>
                                        </div>
                                    </Dropdown.Toggle>

                                    <Dropdown.Menu>
                                        <Dropdown.Item href="#" className="btn" onClick={() => {
                                            sessionStorage.removeItem("__session")
                                            this.setState({ isValidated: false })
                                        }} >Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                    :
                    null
                }
                <div style={{height: "100%"}} >
                    <Room
                        username={this.state.username}
                        jwtToken={this.state.jwtToken}
                        onLeaveRoom={() => this.onLeaveRoom()}
                        onEnterRoom={(roomId) => this.onEnterRoom(roomId)}
                        roomId={this.state.roomId}
                    />
                </div>
            </div>
        )
    }
}

export default Core