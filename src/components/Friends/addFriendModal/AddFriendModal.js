import React, { Component } from 'react'
import { API_ROOT } from '../../../config/api-config'
import './AddFriendModal.css'


export class AddFriendModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchInput: "",
            resultSearch: null,
            errorSearch: "result will be shown up here",
        }
    }

    onSubmitSearchUser = () => {
        let requestBody = {
            friendUsername: this.state.searchInput,
        }
        fetch(`${API_ROOT}/friends/find`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.jwtToken
            },
            body: JSON.stringify(requestBody)
        })
            .then(res => res.json())
            .then(
                (response) => {
                    if (response.error !== undefined) {
                        this.setState({ 
                            errorSearch: response.error,
                            resultSearch: null
                        })
                    } else {
                        this.setState({
                            resultSearch: response
                        })
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    onAddFriend = (username) => {
        let requestBody = {
            friendUsername: username
        }
        fetch(`${API_ROOT}/friends/add`, {
            method: "PUT",
            headers: {
                "Authorization": `Bearer ${this.props.jwtToken}`,
                "content-Type": "application/json"
            },
            body: JSON.stringify(requestBody)
        })
            .then(res => res.json())
            .then(
                (response) => {
                    this.props.onAddedFriend(response)
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    render() {
        if (this.props.addFriend === false) {
            return null;
        }

        let resultFriend;
        if (this.state.errorSearch !== null) {
            resultFriend = (
                <tr>
                    <td>
                        <div>{this.state.errorSearch}</div>
                    </td>
                </tr>
            )
        }
        if (this.state.resultSearch !== null) {
            resultFriend = (
                <tr
                    className="tr-hover"
                    onClick={() => {
                        this.onAddFriend(this.state.resultSearch.username)
                    }}
                >
                    <td>
                        <div>{this.state.resultSearch.username}</div>
                        <div>{this.state.resultSearch.score} <i className="fa fa-trophy"></i></div>
                    </td>
                </tr>
            )
        }
        resultFriend = (
            <table className="table table-responsive table-bordered">
                <tbody>
                    {resultFriend}
                </tbody>
            </table>
        )

        return (
            <div className="add-friend-modal">
                <div className="add-friend-modal-header">Add Friend</div>
                <div className="add-friend-modal-form">
                    <form
                        onSubmit={e => {
                            e.preventDefault();
                            this.onSubmitSearchUser();
                        }}
                    >
                        <div className="search-user">
                            <input
                                onChange={(e) => {
                                    this.setState({
                                        searchInput: e.target.value,
                                    })
                                }}
                                value={this.state.createRoomTitle}
                                className="form-control"
                                type="text"
                                required
                            />
                            <button
                                type="submit"
                                className="btn outline-button"
                            >
                                <i className="fa fa-search"></i>
                            </button>
                        </div>
                    </form>
                    <div className="result-users">
                        {resultFriend}
                    </div>
                    <div className="add-friend-modal-form-buttons">
                        <div>
                            <button
                                onClick={() => this.props.onCancelAddFriend()} className="btn danger-button"
                            >
                                Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default AddFriendModal
