import React, { Component } from 'react'
import './Friends.css'
import AddFriendModal from './addFriendModal/AddFriendModal';
import { API_ROOT } from '../../config/api-config'

export class Friends extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addFriend: false,
            friends: false,
        }
    }

    fetchFriends = () => {
        fetch(`${API_ROOT}/friends/fetch`, {
            headers: {
                "Authorization": `Bearer ${this.props.jwtToken}`,
            }
        })
            .then(res => res.json())
            .then(
                (response) => {
                    this.setState({
                        friends: response.friendState
                    })
                },
                (error) => {
                    console.log(error)
                }
            )
    }

    render() {
        if (this.state.friends === false && this.props.jwtToken !== null) {
            this.fetchFriends();
        }

        let friends;
        if (this.state.friends !== false) {
            if (this.state.friends.length === 0) {
                friends = (
                    <tr>
                        <td>no friend yet :(</td>
                    </tr>
                )
            } else {
                friends = this.state.friends.map((value, index) => {
                    let modal;
                    let onClickIfWaiting, className;
                    if (value.state === "waiting") {
                        onClickIfWaiting = (e) => {
                            if (value.isProtected === true) {
                                this.props.onEnterRoomIdWithPassword(value.roomId)
                            } else {
                                this.props.onEnterRoomIdWithoutPassword(value.roomId)
                            }
                        }
                        className = "tr-friends-list-hover"
                    } else {
                        onClickIfWaiting = (e) => {}
                        className = ""
                    }
                    return (
                        <tr key={index} 
                            className={className}
                            onClick={e => onClickIfWaiting(e)} 
                        >
                            {modal}
                            <th scope="row">
                                <div><img width="30px" src="/friend-image.svg" alt="-"></img></div>
                            </th>
                            <td>
                                <div><span>{value.name}</span></div>
                            </td>
                            <td>
                                <div><span>{value.score} <i className="fa fa-trophy"></i></span></div>
                            </td>
                        </tr>
                    )
                })
            }

            friends = (
                <table className="table table-responsive table-borderless">
                    <tbody>
                        {friends}
                    </tbody>
                </table>
            )
        }

        return (
            <div className="wrapper friends">
                {
                    this.props.pressedFriends ?
                        <div style={{ height: "100%" }}>
                            <AddFriendModal
                                onAddedFriend={(newFriend) => {this.setState({friends: [...this.state.friends, newFriend]})}}
                                onCancelAddFriend={() => { this.setState({ addFriend: false }) }}
                                addFriend={this.state.addFriend}
                                jwtToken={this.props.jwtToken}
                            />
                            <div className="friends-header">
                                <div style={{ width: "50%" }}>
                                    <span>Friends</span>
                                </div>
                                <div className="add-friend"
                                    onClick={() => {
                                        this.setState({ addFriend: !this.state.addFriend })
                                    }}
                                >
                                    <img src="/add-friend.svg" alt="add"></img>
                                </div>
                            </div>
                            <div className="friends-list">
                                {friends}
                            </div>
                        </div>
                        :
                        null
                }
            </div>
        )
    }
}

export default Friends
