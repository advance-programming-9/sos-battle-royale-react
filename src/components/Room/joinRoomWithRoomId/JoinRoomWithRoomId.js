import React, { Component } from 'react'
import { API_ROOT } from '../../../config/api-config'
import './JoinRoomWithRoomId.css'

export class JoinRoomWithRoomId extends Component {
    constructor(props) {
        super(props);
        this.state = {
            joinRoomId: '',
            tempRoomId: -1,
            errorEnterRoom: '',
        }
    }

    fetchGames = () => {
        return fetch(`${API_ROOT}/room/`, {
            headers: {
                'Authorization': 'Bearer ' + this.props.jwtToken
            }
        })
    }

    isValidroomId(roomId) {
        return roomId > 0;
    }

    fetchAndJoinRoom = () => {
        const roomId = Number(this.state.joinRoomId);
        if (!this.isValidroomId(roomId) || this.props.jwtToken === null || this.props.jwtToken === undefined) {
            return;
        }
        this.fetchGames()
            .then(result => result.json())
            .then(
                (response) => {
                    for (let index = 0; index < response.length; index++) {
                        const element = response[index];
                        if (element.roomId === roomId) {
                            if (element.isProtected) {
                                this.props.onEnterRoomWithPasssword(roomId)
                            } else {
                                this.props.onEnterRoom(roomId)
                            }
                            return;
                        }
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    render() {
        if (this.props.pressedJoinRoom === false) {
            return null;
        }
        return (
            <div className="join-room">
                <div className="join-room-header">Join room</div>
                <div className="join-room-form">
                    <form
                        onSubmit={e => {
                            e.preventDefault()
                            this.fetchAndJoinRoom()
                        }}
                    >
                        <div style={{ height: "60%" }}>
                            <div>
                                <label>Room Id</label>
                                <input
                                    onChange={(e) => {
                                        this.setState({
                                            joinRoomId: e.target.value,
                                        })
                                    }}
                                    value={this.state.joinRoomId}
                                    className="form-control"
                                    type="text"
                                    required
                                />
                            </div>
                        </div>
                        <div className="join-room-buttons">
                            <div>
                                <button
                                    onClick={() => this.props.onCancelCreate()} className="btn danger-button"
                                >
                                    Cancel
                                </button>
                            </div>
                            <div>
                                <button
                                    type="submit"
                                    className="btn primary-button"
                                >
                                    Join
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default JoinRoomWithRoomId
