import React, { Component } from 'react'
import './CreateRoomForm.css'

export class CreateRoomForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            chooseCreateRoomWithPassword: false,
            createRoomPassword: "",
            createRoomTitle: "",
        }
    }
    render() {
        if (this.props.pressedCreate === false) {
            return null;
        }
        return (
            <div className="create-room">
                <div className="create-room-header">Create Room</div>
                <div className="create-room-form">
                    <form
                        onSubmit={e => {
                            e.preventDefault()
                            this.props.onSubmitForm(
                                this.state.createRoomTitle,
                                this.state.chooseCreateRoomWithPassword ? this.state.createRoomPassword : ""
                            )
                        }}
                    >
                        <div style={{ height: "60%" }}>
                            <div>
                                <label>Room Title</label>
                                <input
                                    onChange={(e) => {
                                        this.setState({
                                            createRoomTitle: e.target.value,
                                        })
                                    }}
                                    value={this.state.createRoomTitle}
                                    className="form-control"
                                    type="text"
                                    required
                                />
                            </div>
                            <div>
                                <input
                                    style={{ margin: "4px" }}
                                    type="checkbox"
                                    value="withPassword"
                                    checked={this.state.chooseCreateRoomWithPassword}
                                    onChange={() => this.setState({ chooseCreateRoomWithPassword: !this.state.chooseCreateRoomWithPassword })}
                                />
                            with password
                        </div>
                            {
                                this.state.chooseCreateRoomWithPassword ?
                                    <div>
                                        <label>Room Password</label>
                                        <input
                                            onChange={(e) => {
                                                this.setState({
                                                    createRoomPassword: e.target.value,
                                                })
                                            }}
                                            value={this.state.createRoomPassword}
                                            className="form-control"
                                            type="password"
                                            required
                                        />
                                    </div>
                                    :
                                    null
                            }
                        </div>
                        <div className="create-room-buttons">
                            <div>
                                <button
                                    onClick={() => this.props.onCancelCreate()} className="btn danger-button"
                                >
                                    Cancel
                                </button>
                            </div>
                            <div>
                                <button
                                    type="submit"
                                    className="btn primary-button"
                                >
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default CreateRoomForm
