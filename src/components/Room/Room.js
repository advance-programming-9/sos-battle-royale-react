import React, { Component } from 'react'
import { API_ROOT } from '../../config/api-config'
import Game from '../Game/Game'
import './Room.css'
import Loader from 'react-loader-spinner'
import Scoreboard from '../Scoreboard/Scoreboard'
import Friends from '../Friends/Friends'
import CreateRoomForm from './createRoom/CreateRoomForm'
import EnterRoomWithPassword from './enterRoomWithPassword/EnterRoomWithPassword'
import JoinRoomWithRoomId from './joinRoomWithRoomId/JoinRoomWithRoomId'

export class Room extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rooms: undefined,
            password: null,
            tempRoomId: -1,
            pressedScoreboard: false,
            pressedListFriends: false,
            pressedFriends: false,
            pressedCreate: false,
            pressedJoinRoom: false,
            errorEnterRoom: "",
        }
    }

    componentWillReceiveProps = (props) => {
        if (this.props.jwtToken === undefined && props.jwtToken !== undefined) {
            this.setState({
                rooms: undefined,
                password: null,
                pressedScoreboard: false,
                pressedListFriends: false,
            })
        }
    }

    fetchGames = () => {
        fetch(`${API_ROOT}/room/`, {
            headers: {
                'Authorization': 'Bearer ' + this.props.jwtToken
            }
        })
            .then(res => res.json())
            .then(
                (response) => {
                    if (response !== -1) {
                        this.setState({ rooms: response })
                    }
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    createRoom = (title, password) => {
        let requestBody = {
            title: title,
            password: password,
        }
        fetch(`${API_ROOT}/room/create`, {
            method: "PUT",
            headers: {
                'Authorization': 'Bearer ' + this.props.jwtToken,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody),
        })
            .then(res => res.json())
            .then(
                (response) => {
                    this.props.onEnterRoom(response)
                    this.setState({ pressedCreate: false })
                },
                (error) => {
                    console.log(error);
                }
            )
    }

    onLeaveRoom = (subscriptions) => {
        let confirmLeaveGame = window.confirm('Are you sure you want to leave the game?')
        if (confirmLeaveGame === true) {
            this.fetchExitGames()
                .then(res => res.json())
                .then(
                    (res) => {
                        if (res !== -1) {
                            for (let i = 0; i < subscriptions.length; i++) {
                                subscriptions[i].unsubscribe();
                            }
                            this.fetchGames()
                            this.props.onLeaveRoom()
                        }
                    },
                    (err) => {
                        console.log(err);
                    }
                )
        }
    }

    discontinueGame = () => {
        this.fetchExitGames()
            .then(res => res.json())
            .then(
                (res) => {
                    if (res !== -1) {
                        this.fetchGames()
                        this.props.onLeaveRoom()
                    }
                },
                (err) => {
                    console.log(err);
                }
            )
    }

    onEnterRoomWithPasssword = (roomId, password) => {
        let requestBody = {
            roomId: roomId,
            password: password
        }
        fetch(`${API_ROOT}/room/enter`, {
            method: "POST",
            headers: {
                'Authorization': 'Bearer ' + this.props.jwtToken,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(requestBody),
        })
            .then(res => res.json())
            .then(
                (res) => {
                    if (res.error === null) {
                        this.setState({
                            tempRoomId: -1,
                            pressedJoinRoom: false,
                            errorEnterRoom: "",
                        })
                        this.props.onEnterRoom(roomId)
                    }
                    if (res.error === "WRONG_PASSWORD/ROOM_FULL") {
                        this.setState({errorEnterRoom: "wrong password/room full"})
                    }
                },
                (err) => {
                    console.log(err)
                }
            )
    }

    fetchExitGames = () => {
        let jsonform = {
            roomId: this.props.roomId
        }
        return fetch(`${API_ROOT}/room/exit`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.jwtToken
            },
            body: JSON.stringify(jsonform),
        })
    }

    isValidroomId(roomId) {
        return !(roomId === -1 || roomId === undefined || roomId === null);
    }

    playRandomRoom = () => {
        let tempList = [];
        if (this.state.rooms !== undefined) {
            for (let index = 0; index < this.state.rooms.length; index++) {
                const element = this.state.rooms[index];
                if (element.roomResponse.state === "waiting") {
                    tempList.push(element)
                }
            }
        }
        if (tempList.length === 0) {
            this.setState({pressedCreate: true})
            return;
        }
        const pickRandomIndex = Math.floor(Math.random() * tempList.length);
        if (tempList[pickRandomIndex].isProtected) {
            this.setState({tempRoomId: tempList[pickRandomIndex].roomId})
            return
        }
        this.props.onEnterRoom(tempList[pickRandomIndex].roomId)
    }


    render() {
        if (this.state.rooms === undefined && this.props.jwtToken !== null) {
            this.fetchGames()
        }
        let allGames, gameOrLobby;
        if (this.isValidroomId(this.props.roomId)) {
            gameOrLobby = (
                <Game
                    password={this.state.password}
                    username={this.props.username}
                    jwtToken={this.props.jwtToken}
                    discontinueGame={this.discontinueGame}
                    onLeaveRoom={(subscriptions) => this.onLeaveRoom(subscriptions)}
                    roomId={this.props.roomId}
                />
            )
        } else {
            let modal;
            if (this.state.tempRoomId === -1) {
                modal = (
                    <CreateRoomForm
                        onSubmitForm={(title, password) => this.createRoom(title, password)}
                        onCancelCreate={() => this.setState({ pressedCreate: false })}
                        pressedCreate={this.state.pressedCreate}
                    />
                )
            } else {
                modal = (
                    <EnterRoomWithPassword
                        tempRoomId={this.state.tempRoomId}
                        onSubmitForm={(roomId,password) => this.onEnterRoomWithPasssword(roomId,password)}
                        onCancelEnter={() => {this.setState({tempRoomId: -1})}}
                        errorEnterRoom={this.state.errorEnterRoom}
                    />
                )
            }
            if (this.state.rooms !== undefined) {
                allGames = this.state.rooms.map((value, index) => {
                    if (value.roomResponse.state === "waiting") {
                        let onEnterRoomFunc = () => {};
                        if (value.isProtected === true) {
                            onEnterRoomFunc = () => {
                                this.setState({tempRoomId: value.roomId})
                            }
                        } else {
                            onEnterRoomFunc = () => {
                                this.props.onEnterRoom(value.roomId)
                            }
                        }
                        return (
                            <tr
                                onClick={() => onEnterRoomFunc()}
                                key={index}
                            >
                                <th>{value.title}</th>
                                <td>{value.countPeople}/10</td>
                                <td>{value.isProtected ? "Yes" : "No"}</td>
                                <td className="server-row" ><img alt="Idn" src="/indonesia-flag.svg" ></img></td>
                            </tr>
                        )
                    } else {
                        return null;
                    }
                })

                if (allGames.length === 0) {
                    allGames = (
                        <tr
                            onClick={() => this.setState({ pressedCreate: true })}
                        >
                            <td>no one has made a room yet</td>
                        </tr>
                    )
                }

                allGames = (
                    <table className="table table-bordered" >
                        <tbody>
                            {allGames}
                        </tbody>
                    </table>
                )
            } else {
                allGames = (
                    <div style={{
                        display: "flex", alignItems: "center", justifyContent: "center", height: "100%"
                    }}
                        className="text-center" >
                        <Loader type="TailSpin" color="white" width={100} height={100} />
                    </div>
                )
            }
            gameOrLobby = (
                <div style={{ height: "100%" }} >
                    <div className="corner-item" id="trophy-section"
                        onClick={() => {
                            this.setState({
                                pressedScoreboard: !this.state.pressedScoreboard
                            })
                        }}
                    >
                        <img alt="S" src="/trophy.svg"></img>
                    </div>
                    <div className="corner-item" id="friends-section"
                        onClick={() => {
                            this.setState({
                                pressedFriends: !this.state.pressedFriends
                            })
                        }}
                    >
                        <img alt="S" src="/friends.svg"></img>
                    </div>
                    <div style={{ paddingTop: "90px" }} className="text-center">
                        <h3 className="welcome-text" >Welcome to The S|O|S</h3>
                    </div>
                    <div className="wrapper room-wrapper" >
                        <Friends
                            pressedFriends={this.state.pressedFriends}
                            jwtToken={this.props.jwtToken}
                            onEnterRoomIdWithPassword={(roomId) => this.setState({tempRoomId: roomId})}
                            onEnterRoomIdWithoutPassword={(roomId) => this.props.onEnterRoom(roomId)}
                        />
                        <div className="table-rooms" >
                            <div className="all-rooms-header">
                                <table className="table table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th className="row-1" scope="col">Title</th>
                                            <td>Players</td>
                                            <td>Password</td>
                                            <td className="server-row">Server</td>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div className="all-rooms" >
                                {allGames}
                            </div>
                        </div>
                        <Scoreboard
                            pressedScoreboard={this.state.pressedScoreboard}
                            jwtToken={this.props.jwtToken}
                        />
                    </div>
                    {modal}
                    <JoinRoomWithRoomId
                        jwtToken={this.props.jwtToken}
                        onEnterRoomWithPasssword={(roomId) => this.setState({tempRoomId: roomId})}
                        onEnterRoom={(roomId) => this.props.onEnterRoom(roomId)}
                        onCancelCreate={() => this.setState({ pressedJoinRoom: false })}
                        pressedJoinRoom={this.state.pressedJoinRoom}
                    />
                    <div className="create-join-play">
                        <div className="the-buttons">
                            <div>
                                <button
                                    onClick={() => this.setState({ pressedCreate: !this.state.pressedCreate })}
                                    className="btn primary-button"
                                >
                                    Create
                                </button>
                            </div>
                            <div>
                                <button
                                    onClick={() => this.setState({ pressedJoinRoom: !this.state.pressedJoinRoom })}
                                    className="btn primary-button"
                                >
                                    Join
                                </button>
                            </div>
                            <div>
                                <button
                                    onClick={() => this.playRandomRoom()}
                                    className="btn primary-button"
                                >
                                    Play
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div style={{ height: "100%" }} >
                {gameOrLobby}
            </div>
        )
    }
}

export default Room
