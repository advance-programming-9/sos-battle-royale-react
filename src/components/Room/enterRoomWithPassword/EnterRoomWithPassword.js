import './EnterRoomWithPassword.css'
import React, { Component } from 'react'

export class EnterRoomWithPassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            enterPassword: "",
        }
    }

    render() {
        if (this.props.tempRoomId === -1) {
            return null;
        }
        return (
            <div className="enter-room-modal">
                <div className="enter-room-header">Enter Room</div>
                    <div className="enter-room-form">
                        <form
                            onSubmit={e => {
                                e.preventDefault()
                                this.props.onSubmitForm(
                                    this.props.tempRoomId,
                                    this.state.enterPassword
                                )
                            }}
                        >
                            <div style={{ height: "60%" }}>
                                <div>
                                    <label>Enter password</label>
                                    <input
                                        onChange={(e) => {
                                            this.setState({
                                                enterPassword: e.target.value,
                                            })
                                        }}
                                        value={this.state.enterPassword}
                                        className="form-control"
                                        type="password"
                                        required
                                    />
                                    <small style={{color: "red"}} className="form-text">
                                        {this.props.errorEnterRoom}
                                    </small>
                                </div>
                            </div>
                            <div className="enter-room-buttons">
                                <div>
                                    <button
                                        onClick={() => this.props.onCancelEnter()} className="btn danger-button"
                                    >
                                        Cancel
                                    </button>
                                </div>
                                <div>
                                    <button
                                        type="submit"
                                        className="btn primary-button"
                                    >
                                        Enter
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
            </div>
        )
    }
}

export default EnterRoomWithPassword
