import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class AuthForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            form: {
                username: "",
                email: "",
                password: "",
                password2: "",
            }
        }
    }

    static propTypes = {
        onLoginForm: PropTypes.func.isRequired,
        handleChooseLoginOrRegister: PropTypes.func.isRequired,
    }

    render() {

        let usernameStyle = {
            border: (this.props.usernameErrorMsg === null) ?
                '1px solid #379683' : '1px solid red',
        }
        let emailStyle = {
            border: (this.props.emailErrorMsg === null) ?
                '1px solid #379683' : '1px solid red',
        }
        let passwordStyle = {
            border: (this.props.passwordErrorMsg === null) ?
                '1px solid #379683' : '1px solid red',
        }

        if (this.props.invalidLogin !== null && this.props.invalidLogin !== undefined) {
            usernameStyle = {
                border: '1px solid red'
            }

            passwordStyle = usernameStyle

        }

        let smallStyle = {
            color: "red"
        }

        return (
            <div>
                <p
                    className="the-sos"
                >
                    The S|O|S
                </p>
                <p className="wise-word">
                    {
                        this.props.chooseLogin ?
                            'The only place to find Battle Royale S.O.S Game'
                            :
                            'Create a new Account now!'
                    }
                </p>
                <form
                    onSubmit={e => {
                        e.preventDefault()
                        this.props.onLoginForm(this.state.form, this.props.chooseLogin ? 'login' : 'register')
                    }}
                >
                    <div className="form-group">
                        <div>
                            <i className="fa fa-user icon"></i>
                            <input
                                style={usernameStyle}
                                className="form-control"
                                type="text"
                                placeholder={'Username'}
                                value={this.state.form.username}
                                onChange={e => this.setState({
                                    form: {
                                        email: this.state.form.email,
                                        username: e.target.value,
                                        password: this.state.form.password,
                                        password2: this.state.form.password2,
                                    }
                                })}
                                required
                            />
                        </div>
                        <small style={smallStyle} className="form-text">
                            {this.props.invalidLogin || this.props.usernameErrorMsg}
                        </small>
                    </div>
                    {
                        !this.props.chooseLogin ?
                            <div className="form-group">
                                <i className="fa fa-envelope icon"></i>
                                <input
                                    style={emailStyle}
                                    className="form-control"
                                    type="text"
                                    placeholder={'Email'}
                                    value={this.state.form.email}
                                    onChange={e => this.setState({
                                        form: {
                                            email: e.target.value,
                                            username: this.state.form.username,
                                            password: this.state.form.password,
                                            password2: this.state.form.password2,
                                        }
                                    })}
                                    required
                                />
                                <small style={smallStyle} className="form-text">
                                    {this.props.emailErrorMsg}
                                </small>
                            </div>
                            : null
                    }
                    <div className="form-group">
                        <i className="fa fa-lock icon"></i>
                        <input
                            style={passwordStyle}
                            className="form-control"
                            type="password"
                            placeholder={'Password'}
                            value={this.state.form.password}
                            onChange={e => this.setState({
                                form: {
                                    email: this.state.form.email,
                                    username: this.state.form.username,
                                    password: e.target.value,
                                    password2: this.state.form.password2,
                                }
                            })}
                            required
                        />
                        <small style={smallStyle} className="form-text">
                            {this.props.invalidLogin || this.props.passwordErrorMsg}
                        </small>
                    </div>
                    {
                        !this.props.chooseLogin ?
                            <div className="form-group">
                                <i className="fa fa-lock icon"></i>
                                <input
                                    style={passwordStyle}
                                    className="form-control"
                                    type="password"
                                    placeholder={'Re-enter your password'}
                                    value={this.state.form.password2}
                                    onChange={e => this.setState({
                                        form: {
                                            email: this.state.form.email,
                                            username: this.state.form.username,
                                            password: this.state.form.password,
                                            password2: e.target.value,
                                        }
                                    })}
                                    required
                                />
                                <small style={smallStyle} className="form-text">
                                    {this.props.invalidLogin || this.props.passwordErrorMsg}
                                </small>
                            </div>
                            :
                            null
                    }
                    <div className="text-center" >
                        <button id="login-register-button" type="submit" className="btn primary-button">Submit</button>
                    </div>
                </form>
                <br />
                <p>
                    {
                        this.props.chooseLogin ? 'Don\'t have an account yet?' : 'Already have an account?'
                    }
                    <button
                        onClick={e => this.props.handleChooseLoginOrRegister(e)}
                        className="btn btn-link"
                    >
                        {
                            this.props.chooseLogin ? 'Register' : 'Login'
                        }
                    </button>
                </p>
            </div>
        )
    }
}

export default AuthForm
