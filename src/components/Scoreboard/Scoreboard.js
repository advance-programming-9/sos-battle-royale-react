import React, { Component } from 'react'
import Loader from 'react-loader-spinner'
import { API_ROOT } from '../../config/api-config'
import './Scoreboard.css'

export class Scoreboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scoreboards: false,
        }
    }

    getImageMedal(medal) {
        if (medal === null) return null;
        return (
            <img
                alt={medal}
                src={`/${medal}-medal.svg`}
            />
        )
    }

    fetchScoreboard = () => {
        fetch(`${API_ROOT}/scoreboard/fetch`, {
            headers: {
                "Authorization": "Bearer " + this.props.jwtToken
            }
        })
            .then(res => res.json())
            .then(
                (response) => {
                    this.setState({ scoreboards: response })
                },
                (error) => {
                    console.log(error);

                }
            )
    }

    render() {
        if (this.state.scoreboards === false && this.props.jwtToken !== null) {
            this.fetchScoreboard()
        }

        let listScoreboard;
        if (this.state.scoreboards === false) {
            listScoreboard = (
                <Loader type="Oval" />
            )
        } else {
            listScoreboard = this.state.scoreboards.map((value, i) => {
                let index = i + 1

                let medal = null
                let imageMedal = null;
                if (value != null) {
                    if (index === 1) {
                        medal = "gold"
                    }
                    if (index === 2) {
                        medal = "silver"
                    }
                    if (index === 3) {
                        medal = "bronze"
                    }
                }


                imageMedal = this.getImageMedal(medal)

                return (
                    <tr key={i}>
                        <th scope="row">
                            <div><span className="position-number">{index}.</span></div>
                        </th>
                        <td>
                            <div>{imageMedal}</div>
                        </td>
                        <td>
                            <div><span>{value === null ? null : value.username}</span></div>
                        </td>
                        <td>
                            <div>
                                <span
                                    className={medal}
                                >
                                    {value === null ? null : value.score}
                                </span>
                            </div>
                        </td>
                    </tr>

                )
            })
            listScoreboard = (
                <table className="table table-responsive table-borderless">
                    <tbody>
                        {listScoreboard}
                    </tbody>
                </table>
            )
        }

        return (
            <div className="wrapper scoreboard">
                {
                    this.props.pressedScoreboard ?
                    <div>
                        <div className="scoreboard-header">
                            <span>Most Points</span>
                        </div>
                        <div className="scoreboard-list">
                            {listScoreboard}
                        </div>
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}

export default Scoreboard
