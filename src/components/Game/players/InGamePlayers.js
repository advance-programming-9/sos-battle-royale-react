import React from 'react';

function getImageMedal(medal) {
    if (medal === null) return null;
    return (
        <img
            alt={medal}
            src={`/${medal}-medal.svg`}
        />
    )
}

export function InGamePlayers(props) {
    let theClassName;
    if (props.gameState === "waiting") {
        theClassName = "";
    } else {
        theClassName = "rank-players";
    }

    let players;
    if (props.gameState === "waiting") {
        players = props.users.map((value, index) => {
            let crown = "> ";
            let deleteIcon;
            if (value.username === props.roomMaster) {
                crown = (
                    <i style={{ marginRight: "2px" }} className="fas fa-crown"></i>
                )
            }
            return (
                <div className="waiting" key={index}>{crown}{value.username}{deleteIcon}</div>
            )
        })
    } else {
        let sortedUsers = props.sortedUsersByInGameScore.map((value, i) => {
            let index = i + 1

            let medal = null
            let imageMedal = null;
            if (value != null) {
                if (index === 1) {
                    medal = "gold"
                }
                if (index === 2) {
                    medal = "silver"
                }
                if (index === 3) {
                    medal = "bronze"
                }
            }
            imageMedal = getImageMedal(medal)

            return (
                <tr key={index}>
                    <td><div><span>{index}.</span></div></td>
                    <td><div><span>{value === null ? null : value.username}</span></div></td>
                    <td><div><span>{imageMedal}</span></div></td>
                    <td><div><span>{value === null ? null : value.inGameScore}</span></div></td>
                </tr>
            )
        })

        players = (
            <div style={{
                width: "100%",
                height: "100%",
                overflowY: "auto"
            }}>
                <table className="table table-responsive table-borderless">
                    <tbody>
                        {sortedUsers}
                    </tbody>
                </table>
            </div>
        )
    }
    return (
        <div className={`list-players ${theClassName}`}>
            {players}
        </div>
    )
}


export default InGamePlayers
