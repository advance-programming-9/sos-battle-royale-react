import React from 'react'
import './LoadingWebsocket.css'
import Loader from 'react-loader-spinner'

function LoadingWebsocket(props) {
    if (props.subscriptions.length !== 0) {
        return null;
    }
    return (
        <div className="loading-websocker">
            <Loader type="TailSpin" color="white" width={100} height={100} />
            <div className="loading-message">
                <span>Estabilishing websocket connection...</span>
            </div>
        </div>
    )
}

export default LoadingWebsocket