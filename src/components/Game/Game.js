import React, { Component } from 'react'
import Board from '../Board/Board'
import Chat from '../Chat/Chat'
import { API_ROOT, WS } from '../../config/api-config'
import './Game.css'
import SockJS from 'sockjs-client'
import InGamePlayers from './players/InGamePlayers'
import StartLeaveButton from './startLeaveButton/StartLeaveButton'
import WaitingPlayersTurn from './waitingPlayersTurn/WaitingPlayersTurn'
import LoadingWebsocket from './loadingWebsocket/LoadingWebsocket'

export class Game extends Component {
    constructor(props) {
        super(props)
        this.state = {
            squares: Array(10).fill(Array(10).fill(Object)),
            sIsNext: true,
            messages: [],
            gameState: "waiting",
            roomMaster: null,
            countPeople: 0,
            isFinished: null,
            whoIsNext: null,
            users: [],
            sortedUsersByScore: [],
            sortedUsersByInGameScore: [],
            subscriptions: [],
            countDownSeconds: 5,
        }
        this.setIntervalId = null;
    }

    componentDidMount() {
        if (this.props.roomId < 1) {
            return;
        }
        const Stomp = require('@stomp/stompjs')

        let webSocket = new SockJS(WS)

        this.stompClient = Stomp.Stomp.over(webSocket);

        let header = {
            "Authorization": "Bearer " + this.props.jwtToken
        }
        this.stompClient.connect(header, this.onConnected, this.onError);

        let jsonform = {
            roomId: this.props.roomId,
            password: this.props.password,
        }
        fetch(`${API_ROOT}/room/enter`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.jwtToken
            },
            body: JSON.stringify(jsonform),
        })
            .then(res => res.json())
            .then(
                (response) => {
                    if (response.error === null) {
                        this.processGameResponse(response);
                    }
                },
                (error) => {
                    console.log(error);
                }
            );
    }

    processGameResponse = (gameResponse) => {
        this.setState({
            squares                     : gameResponse.arena.grid,
            sIsNext                     : gameResponse.arena.lastChar === "S" ? false : true,
            gameState                   : gameResponse.state,
            roomMaster                  : gameResponse.roomMaster,
            isFinished                  : gameResponse.isFinished,
            countPeople                 : gameResponse.countPeople,
            whoIsNext                   : gameResponse.whoIsNext,
            users                       : gameResponse.users,
            sortedUsersByInGameScore    : gameResponse.sortedUsersByInGameScore,
        })
        if (gameResponse.state === 'playing') {
            clearInterval(this.setIntervalId);
            this.resetCountDown();
            this.countDown();
        }
    }
    
    onClickBoardSquare = () => {
        clearInterval(this.setIntervalId);
    }

    onConnected = () => {
        this.setState({
            subscriptions: [...this.state.subscriptions, 
            this.stompClient.subscribe('/message/room/' + this.props.roomId, this.onMessageReceived),
            this.stompClient.subscribe('/game/room/' + this.props.roomId, this.onGameReceived),    
            ]
        })
    }

    onGameReceived = (response) => {
        response = JSON.parse(response.body);
        this.processGameResponse(response);
    }

    onMessageReceived = (response) => {
        let jsonResponse = JSON.parse(response.body);

        this.addMessage(jsonResponse)
    }

    addMessage = (message) => {
        this.setState(
            { 
                messages: [
                    ...this.state.messages, 
                    message
                ]
            }
        )
    }

    onLeaveRoom = () => {
        this.props.onLeaveRoom(this.state.subscriptions)
    }

    onAddBot = () => {
        let header = {
            "Authorization": "Bearer " + this.props.jwtToken
        }
        this.stompClient.send("/app/game/bot/add/" + this.props.roomId, header)
    }

    onStartGame = () => {
        let jsonform = {
            roomId: this.props.roomId
        }
        fetch(`${API_ROOT}/game/start`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.props.jwtToken
            },
            body: JSON.stringify(jsonform),
        })
    }

    passTurn = () => {
        let header = {
            "Authorization": "Bearer " + this.props.jwtToken
        }
        this.stompClient.send("/app/game/pass/" + this.props.roomId, header)
    }

    countDown = () => {
        this.setIntervalId = setInterval(this.tick, 1000)
    }

    resetCountDown = () => {
        this.setState({
            countDownSeconds: 5,
        })
        this.setIntervalId = null;
    }

    tick = () => {
        let nextCountDown = this.state.countDownSeconds - 1;
        this.setState({
            countDownSeconds: nextCountDown,
        })
        if (nextCountDown === 0) {
            clearInterval(this.setIntervalId);
            if (this.props.username === this.state.whoIsNext) {
                this.passTurn();
            }
        }
    }

    deleteBot = () => {
        let header = {
            "Authorization": "Bearer " + this.props.jwtToken
        }
        this.stompClient.send("/app/game/bot/delete/" + this.props.roomId, header)
    }

    render() {
        return (
            <div className="game">
                <LoadingWebsocket
                    subscriptions={this.state.subscriptions}
                />
                <div className="players-chat">
                    <div className="waiting-score">
                        <WaitingPlayersTurn
                            gameState={this.state.gameState}
                            countPeople={this.state.countPeople}
                            whoIsNext={this.state.whoIsNext}
                            countDownSeconds={this.state.countDownSeconds}
                            />
                        <InGamePlayers 
                            gameState={this.state.gameState}
                            users={this.state.users}
                            sortedUsersByInGameScore={this.state.sortedUsersByInGameScore}
                            roomMaster={this.state.roomMaster}
                            />
                        <StartLeaveButton
                            onStartGame={() => this.onStartGame()}
                            onLeaveRoom={() => this.onLeaveRoom()}
                            onAddBot={() => this.onAddBot()}
                            deleteBot={() => this.deleteBot()}
                            gameState={this.state.gameState}
                            roomMaster={this.state.roomMaster}
                            username={this.props.username}
                            countPeople={this.state.countPeople}
                        />
                    </div>
                    <div className="chat">
                        <Chat
                            messages={this.state.messages}
                            username={this.props.username}
                            roomId={this.props.roomId}
                            jwtToken={this.props.jwtToken}
                            stompClient={this.stompClient}
                        />
                    </div>
                </div>
                <div className="board">
                    <span>Room id: {this.props.roomId}</span>
                    <div className="the-sos the-sos-board">
                        <span>The S|O|S</span>
                    </div>
                    <Board
                        whoIsNext={this.state.whoIsNext}
                        countPeople={this.state.countPeople}
                        gameState={this.state.gameState}
                        username={this.props.username}
                        squares={this.state.squares}
                        sIsNext={this.state.sIsNext}
                        roomId={this.props.roomId}
                        stompClient={this.stompClient}
                        jwtToken={this.props.jwtToken}
                        onClickBoardSquare={() => this.onClickBoardSquare()}
                    />
                </div>
            </div>
        )
    }
}

export default Game