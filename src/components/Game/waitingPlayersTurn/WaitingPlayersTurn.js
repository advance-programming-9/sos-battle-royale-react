import React from 'react'
import Loader from 'react-loader-spinner'

function WaitingPlayersTurn(props) {
    let loaderOrGameIcon, waitingOrWhosTurn, countPlayerOrCountDown;
    if (props.gameState === "waiting") {
        loaderOrGameIcon = (
            <Loader type="TailSpin" color="#EDF5E1" width={50} height={50} />
        )
        waitingOrWhosTurn = (
            <div>Waiting For Another Players...</div>
        )
        countPlayerOrCountDown = (
            <div>{props.countPeople}/10 Players</div>
        )
    } else if (props.gameState === "playing") {
        loaderOrGameIcon = (
            <div><i className="fa fa-gamepad fa-2x"></i></div>
        )
        waitingOrWhosTurn = (
            <div>{props.whoIsNext}'s turn to play</div>
        )
        if (/^\[BOT\]/.test(props.whoIsNext)) {
            countPlayerOrCountDown = (
                <div>
                    <span>
                        <Loader
                            type="TailSpin"
                            color="#EDF5E1"
                            width={50}
                            height={50}
                        />
                    </span>
                </div>
            )
        } else {
            countPlayerOrCountDown = (
                <div>
                    <span style={{color: "red"}}>{props.countDownSeconds}</span>
                    <span> Seconds Left</span>
                </div>
            )
        }

    } else {
        loaderOrGameIcon = (
            <div><i className="fa fa-check fa-2x"></i></div>
        )
        waitingOrWhosTurn = (
            <div>Game Over!</div>
        )
        countPlayerOrCountDown = (
            <div>
                <span style={{color: "red"}}>Congrat</span>
                <span>ulation!</span>
            </div>
        )
    }
    return (
        <div className="waiting-players-turn">
            <div className="waiting-turn">
                <div style={{
                    display: "flex",
                    width: "100%"
                }}>
                    <div style={{
                        width: "115px",
                        height: "94px",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                    }}>
                        {loaderOrGameIcon}
                    </div>
                    <div style={{
                        fontFamily: "Rubik",
                        fontSize: "28px",
                        maxWidth: "345px",
                        padding: "10px",
                    }}>{waitingOrWhosTurn}</div>
                </div>
            </div>
            <div className="count-player-count-down">
                <div>
                    {countPlayerOrCountDown}
                </div>
            </div>
        </div>
    )
}

export default WaitingPlayersTurn
