import React from 'react';

export function StartLeaveButton(props) {
    let leaveButton, startButton, addBotButton, deleteBotButton;
    let widthStyle = {width: "25%"}
    if (props.gameState === "waiting") {
        if (props.username === props.roomMaster) {
            startButton = (
                <div style={widthStyle} className="start-leave">
                    <button
                        className="btn primary-button"
                        disabled={props.countPeople === 1}
                        onClick={props.onStartGame}
                    >
                        Start game
                    </button>
                </div>
            )
            addBotButton = (
                <div style={widthStyle} className="start-leave">
                    <button
                        className="btn primary-button"
                        disabled={props.countPeople >= 10}
                        onClick={props.onAddBot}
                    >
                        add Bot
                    </button>
                </div>
            )
            deleteBotButton = (
                <div style={widthStyle} className="start-leave">
                    <button
                        className="btn primary-button"
                        onClick={props.deleteBot}
                    >
                       Delete Bot 
                    </button>
                </div>
            )
        }
    }
    let disabledLeaveButton;
    if (props.gameState === "playing") {
        disabledLeaveButton = true
    }
    if (props.gameState === "waiting" || props.gameState === "finished") {
        let width;
        if (!(props.gameState === "waiting" && props.roomMaster === props.username)) {
            width = "100%";
        } else {
            width = "25%";
        }
        leaveButton = (
            <div style={{
                width: `${width}`
            }} className="start-leave">
                <button
                    className="btn danger-button"
                    disabled={disabledLeaveButton}
                    onClick={() => props.onLeaveRoom()}
                >
                    Leave game
                </button>
            </div>
        )
    }
    return (
        <div className="start-leave-section">
            {startButton}
            {addBotButton}
            {leaveButton}
            {deleteBotButton}
        </div>
    )
}

export default StartLeaveButton
