let backend, ws;

const hostname = window.location.origin;

if(hostname === 'https://sos.ariqbasyar.id') {
  backend = 'https://sos-api.ariqbasyar.id';
  ws = 'https://sos-api.ariqbasyar.id/socket';

} else if (hostname === 'http://localhost:21483') {
  backend = 'http://localhost:21484';
  ws = 'http://localhost:21484/socket';

} else {
  backend = 'http://localhost:8080';
  ws = 'http://localhost:8080/socket';
}

export const API_ROOT = `${backend}`;
export const WS = `${ws}`;