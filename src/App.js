import React, { Component } from 'react'

import Core from './components/Core/Core'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'


export class App extends Component {
  render() {
    return (
      <div style={{height: "100%"}} >
        <Core />
      </div>
    )
  }
}


export default App;
