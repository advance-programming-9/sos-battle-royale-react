import React from 'react';
import { render } from '@testing-library/react';
import App from '../App';

import {BrowseRouter as Router} from 'react-router-dom';
import { shalow } from 'enzyme';


test('renders learn react link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

// describe('<App /', () => {
//   it ('renders on <Router /> component', () => {
//     const wrapper = shallow(<App />);
//     expect (wrapper.find(Router)).toHaveLength(1);
//   });
// });
